package configs

import (
	"database/sql"
	"fmt"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

type config struct {
	db_connection string
	db_host       string
	db_username   string
	db_password   string
	db_port       string
	db_name       string
}

func connection_map() config {
	conf := config{
		db_connection: os.Getenv("DB_CONNECTION"),
		db_host:       os.Getenv("DB_HOST"),
		db_username:   os.Getenv("DB_USERNAME"),
		db_password:   os.Getenv("DB_PASSWORD"),
		db_port:       os.Getenv("DB_PORT"),
		db_name:       os.Getenv("DB_NAME"),
	}
	return conf
}

func connection_string() string {
	conf := connection_map().db_username + ":" +
		connection_map().db_password + "@tcp(" +
		connection_map().db_host + ":" +
		connection_map().db_port + ")/" +
		connection_map().db_name + "?charset=utf8&parseTime=True&loc=Local"
	return conf
}

func Init() *sql.DB {
	InDB, err := sql.Open(connection_map().db_connection, connection_string())
	if err != nil {
		panic("failed to connect to database")
	}
	fmt.Println("database connected")
	return InDB
}
