package Services

import (
	"fmt"
	libraries "redcat_services/app/Http/Libraries"
	models "redcat_services/app/Http/Models"
	"redcat_services/configs"
	"unsafe"
)

func GetAppUi(CreatedBy int64) bool {
	connection := configs.Init()
	var result = *&models.MerchantDefaultImages{}
	ImageFilename := fmt.Sprintf("default-%d.png", CreatedBy)
	sql := "SELECT id, plu FROM app_ui_images WHERE created_by = ? AND image_filename = ? AND is_active = 1 AND is_deleted = 0"

	err := connection.QueryRow(sql, CreatedBy, ImageFilename).Scan(&result)
	libraries.CheckError(err)

	defer connection.Close()

	if unsafe.Sizeof(result) > 0 {
		return true
	}
	return false
}
