package Services

import (
	"fmt"
	helpers "redcat_services/app/Http/Helpers"
	libraries "redcat_services/app/Http/Libraries"
	"redcat_services/app/Http/Models"
	structs "redcat_services/app/Http/Structs"
	"redcat_services/configs"
)

func GetDish(object *structs.StreamData) []Models.Dishes {
	connection := configs.Init()

	sql := "SELECT id, plu FROM dishes WHERE created_by = %d AND plu IN (%s)"
	var whereIn string = helpers.ArrayToString(object.Plus, ",")

	sql = fmt.Sprintf(sql, object.Id, whereIn)

	fmt.Println("\n#############################################")
	fmt.Println("QUERY EXECUTE :")
	fmt.Println(sql)
	fmt.Println("#############################################\n")

	rows, err := connection.Query(sql)

	libraries.CheckError(err)

	defer connection.Close()

	var result []Models.Dishes

	for rows.Next() {
		var id int64
		var plu string

		err := rows.Scan(&id, &plu)
		result = append(result, Models.Dishes{Id: id, Plu: plu})
		libraries.CheckError(err)

	}
	fmt.Printf("List Dish : %s\n", result)

	return result
}

func UpdateImage(outletID int64, plu string, imageName string) {
	connection := configs.Init()
	sql := "update dishes set image = ? where plu = ? and created_by = ?"

	res, err := connection.Exec(sql, imageName, plu, outletID)
	libraries.CheckError(err)

	a, e := res.RowsAffected()
	libraries.CheckError(e)
	fmt.Printf("affected rows : %d \n", a)
}
