package Services

import (
	"encoding/json"
	"fmt"
	"redcat_services/app/Http/Models"
	"redcat_services/configs"
)

func GetUsersInfo(id int64) *Models.Users {
	connection := configs.Init()
	var result = Models.Users{}

	sql := `SELECT 
		u.id, u.name, u.username, ur.redcat_store_id, u.lattitude, u.longitude, u.time_zone, ur.api_url 
	FROM user_redcat_mapping ur 
	JOIN users u ON u.id = ur.id 
	WHERE ur.id = ?;`

	fmt.Println("\n#############################################")
	fmt.Println("QUERY EXECUTE:")
	fmt.Printf("%s \n", sql)
	fmt.Println("#############################################\n")

	row := connection.QueryRow(sql, id)
	err := row.Scan(&result.Id, &result.Name, &result.Username, &result.RedcatStoreId, &result.Lattitude, &result.Longitude, &result.Timezone, &result.ApiUrl)

	if err != nil {
		fmt.Println(err)
		return nil
	}
	defer connection.Close()

	// fmt.Printf("test id: %v, name: %v \n", result.Id, result.Name)
	pagesJson, err := json.Marshal(result)

	fmt.Println("\n#############################################")
	fmt.Println("JSON RESULT:")
	fmt.Printf("%s \n", pagesJson)
	fmt.Println("#############################################\n")

	return &result
}
