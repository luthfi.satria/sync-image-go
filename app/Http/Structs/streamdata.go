package Structs

type StreamData struct {
	Id         int64             `json:"id"`
	Parameters map[string]string `json:"parameters"`
	Plus       []int64           `json:"plus"`
}
