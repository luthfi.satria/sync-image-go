package Libraries

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin"
)

func CheckError(err error) gin.H {
	if err != nil {
		return gin.H{
			"status": false,
			"err":    err,
		}
	}
	return nil
}

func PrettyPrintObj(Obj []byte) {
	prettyJson, err := json.MarshalIndent(Obj, "", "\t")
	CheckError(err)
	fmt.Printf("Response : %v \n", string(prettyJson))
}

func PrettyPrintMap(Obj map[string]string) {
	prettyJson, err := json.MarshalIndent(Obj, "", "\t")
	CheckError(err)
	fmt.Printf("Response : %v \n", string(prettyJson))
}
