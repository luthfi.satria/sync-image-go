package Controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"unsafe"

	helpers "redcat_services/app/Http/Helpers"
	libs "redcat_services/app/Http/Libraries"
	"redcat_services/app/Http/Models"
	services "redcat_services/app/Http/Services"
	structs "redcat_services/app/Http/Structs"

	// "log"

	"github.com/disintegration/imaging"
	"github.com/gin-gonic/gin"
	"github.com/labstack/echo"
)

var client = &http.Client{}

var ImageList []string

func CatchingRequest(c echo.Context) (err error) {
	fmt.Println("************ Images.go -> StartSync *********************")

	sd := new(structs.StreamData)
	if err = c.Bind(sd); err != nil {
		return
	}

	// getting user information
	users := services.GetUsersInfo(sd.Id)

	// getting dish information
	dishes := services.GetDish(sd)

	// Collect PLUS
	var ImagePlus map[string]int64

	if len(dishes) > 0 {
		ImagePlus = make(map[string]int64)
		for _, value := range dishes {
			ImagePlus[value.Plu] = value.Id
		}
	}

	result := gin.H{
		"users":      users,
		"dishes":     dishes,
		"parameters": sd.Parameters,
		"listplus":   ImagePlus,
	}

	result = StartSync(result)
	return c.JSON(http.StatusOK, result)
}

func CheckingDb(c echo.Context) error {
	result := gin.H{
		"result": "OK",
	}
	return c.JSON(http.StatusOK, result)
}

func LoggingStatus(c echo.Context) error {
	result := gin.H{
		"result": "OK",
	}
	return c.JSON(http.StatusOK, result)
}

func StartSync(request map[string]interface{}) gin.H {
	fmt.Printf("length of request :%d \n", unsafe.Sizeof(request))
	if unsafe.Sizeof(request) > 0 {
		listPlus := request["listplus"].(map[string]int64)
		fmt.Println("************ Images.go -> StartSync *********************")
		fmt.Println("listPlus :")
		fmt.Println(listPlus)

		userData := request["users"].(*Models.Users)
		url, err := url.Parse(userData.ApiUrl)

		libs.CheckError(err)

		ImagePath := url.Scheme + "://" + url.Host + "/static/img/ordering/"
		// fmt.Printf("ImagePath : %s \n", ImagePath)

		//checking existing merchant default images
		CreatedBy := userData.Id
		DefaultExists := services.GetAppUi(CreatedBy)

		// REQUEST IMAGES AND UPDATE AND COPY IMAGE INTO TEMPORARY FOLDERS
		ReqImg := RequestImages(userData, listPlus, ImagePath, DefaultExists)

		// RESIZE IMAGES
		ResizeImages(request, ReqImg)
	}
	return nil
}

func RequestImages(userData *Models.Users, listPlus map[string]int64, ImagePath string, DefaultImageExists bool) []map[string]string {
	uploadsDir := os.Getenv("UPLOAD_DIR") + os.Getenv("ORIGINAL_FOLDER")

	// var ImageItem string
	// var itemData, defaultData []map[string]interface{}
	var Result []map[string]string
	for key, value := range listPlus {
		fmt.Println("---------- START LOOP List Plus ----------")

		response, err := client.Get(userData.ApiUrl + "/" + key)
		libs.CheckError(err)

		defer response.Body.Close()

		apiResponse, err := ioutil.ReadAll(response.Body)
		libs.CheckError(err)

		// decode JSON
		jsonResponse := make(map[string]interface{})
		err = json.Unmarshal([]byte(apiResponse), &jsonResponse)
		jsonResponse = jsonResponse["data"].(map[string]interface{})

		ImageLarge := jsonResponse["ImageLarge"].(string)

		var ImageName string
		var ImageSize int64

		if ImageLarge != "" {
			ImageName = fmt.Sprintf("%d_%d_dish_%s", userData.Id, value, ImageLarge)
			ImageSize = CopyImage(ImagePath+ImageLarge, uploadsDir, ImageLarge)

			if ImageSize == 0 {
				ImageName = fmt.Sprintf("default-%d.png", userData.Id)
			}
		} else if ImageLarge == "" && DefaultImageExists == true {
			ImageName = fmt.Sprintf("default-%d.png", userData.Id)
		} else {
			ImageName = ""
		}

		// UPDATE IMAGES IN dishes TABLE
		services.UpdateImage(userData.Id, key, ImageName)

		var LogPrints map[string]string
		LogPrints = map[string]string{
			"Key":            key,
			"UploadDir":      uploadsDir,
			"MenuURL":        userData.ApiUrl + "/" + key,
			"ImageURL":       ImagePath,
			"ImageLarge":     ImageLarge,
			"ConvertingName": ImageName,
			"ImageSize":      fmt.Sprintf("%v", ImageSize),
		}
		// libs.PrettyPrintMap(LogPrints)
		fmt.Println("---------- END LOOP List Plus ------------")
		Result = append(Result, LogPrints)
	}
	return Result
}

func CopyImage(url string, UploadsDir string, fileName string) int64 {
	fileName = UploadsDir + fileName

	fmt.Printf("get Image from URL : %s \n", url)

	getImage, err := client.Get(url)
	libs.CheckError(err)

	defer getImage.Body.Close()

	fmt.Printf("get Image status : %d \n", getImage.StatusCode)
	if getImage.StatusCode == 200 && err == nil {
		// create dir if not exist
		helpers.CreateDir(UploadsDir, 0777)

		// copy image into temporary folder
		size := helpers.CreateFile(fileName, getImage.Body)

		return size
	}
	return 0
}

func ResizeImages(request map[string]interface{}, ReqImg []map[string]string) {
	// Resizing Images into different size
	Size := Models.Size
	fmt.Sprintf("%s", Size)

	for _, value := range ReqImg {
		libs.PrettyPrintMap(value)
		ImgPath := fmt.Sprintf("%s%s", value["UploadDir"], value["ImageLarge"])
		// regex, _ := regexp.Compile(`\.(gif|jpe?g|png)$`)
		// ImgExt := regex.FindString(value["ImageLarge"])
		// fmt.Println(ImgExt)

		var _, err = os.Stat(ImgPath)
		if os.IsNotExist(err) {
			fmt.Println("Not Exists")
		} else {
			// fmt.Println("Exists")
			// file, err := os.Open(ImgPath)
			// libs.CheckError(err)
			// defer file.Close()

			for key, val := range Size {
				newDir := os.Getenv("UPLOAD_DIR") + "/" + key
				newFile := newDir + "/" + value["ConvertingName"]
				// create directories
				helpers.CreateDir(newDir, 0777)

				// copy file
				helpers.CloneFile(ImgPath, newFile, 0777)

				// resize file
				file, err := imaging.Open(newFile)
				libs.CheckError(err)

				src := imaging.Resize(file, 50, 0, imaging.Lanczos)

				imaging.Save(src, newFile)

				fmt.Printf("%s", val["width"])
			}
			// file, err := os.Open(ImgPath)
			// libs.CheckError(err)
			// defer file.Close()

			// img, _, err := image.Decode(file)
			// libs.CheckError(err)

			// // resizing images
			// newImage := resize.Resize(160, 0, original_image, resize.Lanczos3)
		}
	}

	// UPLOAD IMAGES INTO CLOUD STORAGE
	Parameters := request["parameters"].(map[string]string)
	// fmt.Printf("%s", Parameters["IS_AWS"])

	if Parameters["IS_AWS"] == "TRUE" {

	} else {

	}
}
