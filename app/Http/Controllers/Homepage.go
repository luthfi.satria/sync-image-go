package Controllers

import (
	"net/http"

	"github.com/labstack/echo"
)

func Index(c echo.Context) error {
	return c.JSON(http.StatusOK, "homepage")
}
