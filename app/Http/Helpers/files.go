package Helpers

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	libs "redcat_services/app/Http/Libraries"
)

func CreateDir(Dirname string, Permission os.FileMode) {
	// create dir if not exist
	os.MkdirAll(Dirname, Permission)
}

func CreateFile(fileName string, getImage io.ReadCloser) int64 {
	file, err := os.Create(fileName)
	libs.CheckError(err)
	// copy image into temporary folder

	size, err := io.Copy(file, getImage)
	defer file.Close()
	libs.CheckError(err)

	fmt.Printf("Just Downloaded a file %s with size %d\n", fileName, size)
	return size
}

func CloneFile(sourceFile string, newFile string, FilePerm os.FileMode) {
	input, err := ioutil.ReadFile(sourceFile)
	libs.CheckError(err)

	// clone file
	err = ioutil.WriteFile(newFile, input, FilePerm)
	libs.CheckError(err)
}
