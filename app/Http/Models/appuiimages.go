package Models

type MerchantDefaultImages struct {
	Id             int64
	ImageLocation  string
	ImageFilename  string
	ImageTitle     string
	IsActive       bool
	IsDeleted      bool
	LastUpdate     string
	CreatedBy      int64
	Created        string
	Modified       string
	IsEmenuImage   bool
	IsSkipqueImage bool
	IsEmenuAndroid bool
	IsKioskImage   bool
	IsSmartweb     bool
}

type ImageDimension map[string]map[string]int64

var Size = ImageDimension{
	"small": map[string]int64{
		"width":  110,
		"height": 110,
	},
	"medium": map[string]int64{
		"width":  226,
		"height": 226,
	},
	"large": map[string]int64{
		"width":  640,
		"height": 960,
	},
	"smartweb": map[string]int64{
		"width":  500,
		"height": 500,
	},
}
