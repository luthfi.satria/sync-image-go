package Models

type Users struct {
	Id            int64  `json:"Id"`
	Name          string `json:"Name"`
	Username      string `json:"Username"`
	RedcatStoreId int64  `json:"RedcatStoreId"`
	Lattitude     string `json:"Lattitude"`
	Longitude     string `json:"Longitude"`
	Timezone      string `json:"Timezone"`
	ApiUrl        string `json:"ApiUrl"`
}
