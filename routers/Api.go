package routers

import (
	"redcat_services/app/Http/Controllers"

	"os"

	"github.com/labstack/echo"
)

func Api() {
	e := echo.New()
	e.GET("/index", Controllers.Index)
	e.POST("/caching_request", Controllers.CatchingRequest)
	e.GET("/checking_db", Controllers.CheckingDb)
	e.GET("/logging_status", Controllers.LoggingStatus)

	e.Logger.Fatal(e.Start(":" + os.Getenv("APP_PORT")))
}
