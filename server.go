package main

import (
	"redcat_services/routers"

	"github.com/joho/godotenv"
)

func main() {
	godotenv.Load()
	routers.Api()
}
